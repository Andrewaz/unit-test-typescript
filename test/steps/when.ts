// test when the user sent any type of request

import { RankingFinder } from "../../src/ranking-finder/models/ranking";



const axios = require('axios').default;



export const get_ranking_request = async (number: number, language: string) => {
try {
    const response = await axios.get(`https://raw.githubusercontent.com/EvanLi/Github-Ranking/master/Data/github-ranking-2018-12-18.csv`);
    const finder = new RankingFinder(number, language);
    const refactoredData = await finder.readFetchedData(response.data);
    finder.setRefactoredData(refactoredData);
    finder.searchRepositoryByNumber();
    const result = finder.formatOutput();
    return { message: 'The request was succesfully', data: result };
} catch (error) {
    return { message: error, data: null };
}
};

