// we are waiting for an authenticated user

import * as AWS from 'aws-sdk';
AWS.config.region = 'us-east-1';
const cognito = new AWS.CognitoIdentityServiceProvider();
import * as jwt from 'jsonwebtoken';


 export  const authenticated_aws_user = async () => {
    const userPoolId = process.env.USER_POOL_ID;
    const clientId = process.env.CLIENT_ID;
    const userName = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const params = {
        UserPoolId: userPoolId,
        ClientId: clientId,
        AuthFlow: 'ADMIN_NO_SRP_AUTH',
        AuthParameters: {
            USERNAME: userName,
            PASSWORD: password
        }
    }

    let user = await cognito.adminInitiateAuth(params).promise();
    return user;
}

export  const authenticate_jwt_user = () => {
    const jwtKey = process.env.JWT_KEY;
    const jwtExpirySeconds = process.env.JWT_EXPIRY_SECONDS;
    const username = process.env.JWT_USERNAME;
    const password = process.env.JWT_PASSWORD;

    // Create a new token with the username in the payload
    // and which expires seconds after issue
    const token = jwt.sign({ username }, jwtKey, {
        algorithm: "HS256",
        expiresIn: jwtExpirySeconds
    })
    return token;
}

export  const verify_jwt_user = () => {
    try {
        const jwtKey = process.env.JWT_KEY;
        const jwtExpirySeconds = process.env.JWT_EXPIRY_SECONDS;
        const token = process.env.JWT_TOKEN;
        jwt.verify(token, jwtKey);
        return true;
    } catch (e) {
        if (e instanceof jwt.JsonWebTokenError) {
            // if the error thrown is because the JWT is unauthorized, return a 401 error
            throw (e)
        }

    }
}