import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import {init}  from './steps/init';
import { authenticated_aws_user, authenticate_jwt_user, verify_jwt_user } from './steps/given';
import { get_ranking_request } from './steps/when';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });


  describe('Authenticate JWT user', () => {
    init();
    let token = authenticate_jwt_user();
    console.log('New JWT token:', token);
  });
  
  describe('Given an JWT authenticated user', () => {
    init();
    let authenticated_user = verify_jwt_user();
    it('Should expect to a valid and authenticated jwt user', () => {
        expect(authenticated_user).toBe(true);
    });
  
    it('Should expect to a valid string output', async () => {
        const { message, data } = await get_ranking_request(2, 'JavaScript');
        expect(message).toEqual('The request was succesfully');
    });
  });



  afterAll(()=> {
    app.close();
  })
});




describe('Authenticate JWT user', () => {
  init();
  let token = authenticate_jwt_user();
  console.log('New JWT token:', token);
});


