// load env variables
const init = async () => {
    require('dotenv').config();
}

module.exports = init;